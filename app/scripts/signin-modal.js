import $ from 'jquery';
import firebaseEvents from './firebaseEvents';
import getDomDataAttributes from '../components/util/domAttributes-util';

function attachListeners() {
    firebaseEvents.once('loaded', () => {
        require.ensure(['./firebase'], (require) => {
            const createFirebaseApp = require('./firebase').default;

            firebaseEvents.on('login', ({ year }) => {
                const $modal = $(`#AccountModal-${year}`);
                $modal.find('.choose-provider').css('display', '');
                $modal.find('.account-modal-body__intro-txt').css('display', '');
                $modal.find('.signin-error').text('');
            });

            $('.sign-in-button--facebook').on('click', (event) => {
                event.preventDefault();
                const year = $(event.target).closest('.modal').data('year');
                const fb = createFirebaseApp(year);
                const provider = new fb.ns.auth.FacebookAuthProvider();
                provider.addScope('email');
                const $modal = $(`#AccountModal-${year}`);
                fb.auth.signInWithPopup(provider)
                    .then(() => {
                        if ($modal.data('inpromise') !== true) {
                            $modal.find('.loading--placeholder').css('display', 'block');
                            window.location.href = '/my-conference/';
                        }
                    })
                    .catch((error) => {
                        $modal.find('.signin-error').text('Error Signing In');
                    });
            });

            $('.sign-in-button--google').on('click', (event) => {
                event.preventDefault();
                const year = $(event.target).closest('.modal').data('year');
                const fb = createFirebaseApp(year);
                const provider = new fb.ns.auth.GoogleAuthProvider();
                provider.addScope('email');
                const $modal = $(`#AccountModal-${year}`);
                fb.auth.signInWithPopup(provider)
                    .then(() => {
                        if ($modal.data('inpromise') !== true) {
                            $modal.find('.loading--placeholder').css('display', 'block');
                            window.location.href = '/my-conference/';
                        }
                    })
                    .catch((error) => {
                        $modal.find('.signin-error').text('Error Signing In');
                    });
            });

            $('.sign-in-button--email').on('click', (event) => {
                event.preventDefault();
                const year = $(event.target).closest('.modal').data('year');
                const $modal = $(`#AccountModal-${year}`);
                $modal.find('.choose-provider').css('display', 'none');
                $modal.find('.account-modal-body__intro-txt').css('display', 'none');
                $modal.find('.loading--placeholder').css('display', 'block');
                $modal.find('.signin-error').text('');
                require.ensure(['./react-app'], () => {
                    require.ensure(['../components/EmailSignin'], () => {
                        const App = require('../components/EmailSignin').default;
                        const el = $modal.find('.email-signin-mount-point').get(0);
                        App.config(getDomDataAttributes(el), el);
                        $('.loading--placeholder').css('display', '');
                    }, 'emailSigninChunk');
                }, 'reactChunk');
            });

            $('.email-signin-mount-point').on('click', '.login-form__back-btn', (event) => {
                event.preventDefault();
                const year = $(event.target).closest('.modal').data('year');
                const $modal = $(`#AccountModal-${year}`);
                $modal.find('.choose-provider').css('display', 'block');
                $modal.find('.account-modal-body__intro-txt').css('display', 'block');
                $modal.find('.account-modal .modal-title').text('Account');
            });
        }, 'fbChunk');
    });
}

export default {
    init() {
        attachListeners();
    },
};
