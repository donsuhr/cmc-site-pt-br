import Basil from 'basil.js';
import $ from 'jquery';
import firebaseEvents from './firebaseEvents';

const AUTH_KEY = 'authenticated';
const USER_KEY = 'user';

const basil = new Basil({
    storages: ['session', 'cookie', 'memory'],
    expireDays: 1,
});

function doSignOut(firebaseAuth, disableRedirect) {
    firebaseAuth.signOut().then(() => {
        $('#AccountModal').modal('hide');
        basil.set(AUTH_KEY, false);
        if (!disableRedirect) {
            window.location.href = '/';
        }
    });
}

function updateView(year = '2017') {
    const isAuthenticated = basil.get(`${AUTH_KEY}-${year}`) === true;
    if (isAuthenticated) {
        document.body.classList.add(`authenticated-${year}`);
        const email = basil.get(`${USER_KEY}-${year}`);
        const emailTextEl = document.querySelector('.authenticated-user-email');
        if (emailTextEl) {
            emailTextEl.innerText = email;
        }
    } else {
        document.body.classList.remove(`authenticated-${year}`);
    }
    document.body.classList.remove('auth-loading');
}

function attachListeners() {
    $('.sign-out-button').on('click', (event) => {
        event.preventDefault();
        document.body.classList.add('auth-loading');
        const year = $(event.target).data('year');
        const disableRedirect = $(event.target).data('disable-redirect');
        require.ensure(['./firebase'], (require) => {
            const createFirebaseApp = require('./firebase').default;
            const fb = createFirebaseApp(year);
            const firebaseAuth = fb.auth;
            doSignOut(firebaseAuth, disableRedirect);
        }, 'fbChunk');
    });
    firebaseEvents.on('login', ({ year, user }) => {
        basil.set(`${AUTH_KEY}-${year}`, true);
        basil.set(`${USER_KEY}-${year}`, user.email);
        updateView(year, user.email);
    });
    firebaseEvents.on('logout', ({ year }) => {
        basil.set(`${AUTH_KEY}-${year}`, false);
        basil.set(`${USER_KEY}-${year}`, false);
        updateView(year);
    });

    $('[id^="AccountModal"]').one('show.bs.modal', (event) => {
        const year = $(event.target).data('year');
        const $modal = $(`#AccountModal-${year}`);
        const $loader = $modal.find('.loading--placeholder');
        $loader.show();
        require.ensure(['./firebase'], (require) => {
            const createFirebaseApp = require('./firebase').default;
            const firebaseAuth = createFirebaseApp(year).auth;
            const cancel = firebaseAuth.onAuthStateChanged((user) => {
                if (user) {
                    if ($modal.data('inpromise') !== true) {
                        // $loader.show();
                        // window.location.href = '/my-conference/';
                        console.log('auto redirect');
                    }
                }
            });
            setTimeout(cancel, 2000);
            $loader.hide();
        }, 'fbChunk');
    });
}

export default {
    init() {
        attachListeners();
        updateView('2017');
        updateView('2018');
        updateView('2019');
        updateView('2020');
    },
};
