import getDomDataAttributes from '../../components/util/domAttributes-util';

require.ensure(
    ['../firebase'],
    () => {
        require.ensure(
            ['../react-app'],
            () => {
                require.ensure(
                    ['../../components/SupportConsultationsImport'],
                    (require) => {
                        const App = require('../../components/SupportConsultationsImport')
                            .default;
                        const el = document.getElementById('CiAppMountPoint');
                        const dataAttributes = getDomDataAttributes(el);
                        App.config(dataAttributes, el);
                    },
                    'scheduleImportChunk',
                );
            },
            'reactChunk',
        );
    },
    'fbChunk',
);
