import getDomDataAttributes from '../../components/util/domAttributes-util';

require.ensure(['../firebase'], () => {
    require.ensure(['../react-app'], () => {
        require.ensure(['../../components/SupportConsultationsDlXls'], (require) => {
            const App = require('../../components/FirebaseTest').default;
            const el = document.getElementById('CiAppMountPoint');
            const dataAttributes = getDomDataAttributes(el);
            App.config(dataAttributes, el);
        }, 'firebaseTestChunk');
    }, 'reactChunk');
}, 'fbChunk');
