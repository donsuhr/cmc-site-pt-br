/* eslint-disable no-underscore-dangle */

import $ from 'jquery';
import config from 'config';
import queryString from 'query-string';
import form from 'cmc-site/app/components/form';
import googleMaps from '../../components/google-maps--contact-us';

googleMaps.init();

const $el = $('#requestDemoForm');
const endpoint = $el.attr('action');
const successRedirectUrl = $el.data('success-redirect-url') || '/contact-us/contact-complete/';

const _form = form.createForm({
    leadSource: 'contact us form',
    successRedirectUrl,
    formDomID: 'requestDemoForm',
    endpoint,
    formalizerOptions: {
        id: config.leadLanderID,
        pairs: {
            // llname : existing element name
            email: 'email',
            firstName: 'firstName',
            lastName: 'lastName',
            phone: 'phone',
        },
    },
});

const _$el = _form.getEl();

function updateHiddenFields() {
    const parsed = queryString.parse(window.location.search);
    const { reason } = parsed;
    if (reason) {
        _$el.find('[name^="reasonForInquiry"]').val(reason);
        _$el.find('[name^="Motivo de la consulta"]').val(reason);
    }
}

updateHiddenFields();
