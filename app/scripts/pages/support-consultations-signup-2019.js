import getDomDataAttributes from '../../components/util/domAttributes-util';

require.ensure(['../firebase'], () => {
    require.ensure(['../react-app'], () => {
        require.ensure(['../../components/SupportConsultationsSignup2019'], (require) => {
            const App = require('../../components/SupportConsultationsSignup2019').default;
            const el = document.getElementById('SupportConsultationsMountPoint');
            const dataAttributes = getDomDataAttributes(el);
            App.config(dataAttributes, el);
        }, 'supportConsultationsChunk');
    }, 'reactChunk');
}, 'fbChunk');
