import signInButtonApi from '../signin-button';
import signInModalApi from '../signin-modal';
import firebaseEvents from '../firebaseEvents';

firebaseEvents.on('login', ({ user, year }) => {
    require.ensure(['../firebase'], (require) => {
        const createFirebaseApp = require('../firebase').default;
        const fb = createFirebaseApp(year);
        const usersRef = fb.db.ref('users');
        const userRef = usersRef.child(user.uid);
        const providerDataRef = userRef.child('providerData');
        userRef.once('value', (userData) => {
            const hasUserKey = userData.val() !== null;
            if (hasUserKey) {
                providerDataRef.once('value', (providerData) => {
                    const hasDataKey = providerData.val() !== null;
                    if (!hasDataKey) {
                        providerDataRef.set(user.providerData[0]);
                    }
                });
            } else {
                // no data at all for user
                providerDataRef.set(user.providerData[0]);
            }
        });
    }, 'fbChunk');
});

signInButtonApi.init();
signInModalApi.init();
