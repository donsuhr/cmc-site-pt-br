import * as authActions from './auth.actions';
import * as actions from './listeners.actions';

const listeners = (state = [], action) => {
    switch (action.type) {
        case actions.LISTENER_ADDED:
            return [
                ...state,
                action.ref,
            ];
        case authActions.SIGN_OUT_SUCCESS:
        case authActions.PASSIVE_SIGN_OUT:
            state.forEach((x) => {
                x.off();
            });
            return [];
        default:
            return state;
    }
};

export function getListenerByRef(state, ref) {
    if (!state || !state.listeners) {
        return null;
    }
    const filtered = state.listeners.filter(x => x.toString() === ref.toString());
    return filtered.length > 0 ? filtered[0] : null;
}

export default listeners;
