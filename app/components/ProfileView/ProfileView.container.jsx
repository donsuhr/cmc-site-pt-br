import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { listenToAuth } from '../firebase/auth.actions';
import { listenToProfile } from '../firebase/profile.actions';

import ProfileView from './ProfileView';

export class ProfileViewContainer extends React.Component {
    static propTypes = {
        profile: PropTypes.shape({
            info: PropTypes.shape({
                firstName: PropTypes.string,
                lastName: PropTypes.string,
            }),
            loadStatus: PropTypes.string,
        }),
        listenToAuthConnect: PropTypes.func,
        listenToProfileConnect: PropTypes.func,
    };

    componentDidMount() {
        const { listenToAuthConnect, profile, listenToProfileConnect } = this.props;
        listenToAuthConnect().then((user) => {
            if (profile.loadStatus !== 'success') {
                listenToProfileConnect(user);
            }
        });
    }

    render() {
        const { profile } = this.props;
        const showProfile = profile.info.firstName
            && profile.info.lastName;
        if (showProfile) {
            return <ProfileView profile={profile} />;
        }
        return null;
    }
}

function mapStateToProps(state, ownProps) {
    return {
        profile: state.profile,
    };
}

const ProfileViewConnected = connect(
    mapStateToProps,
    {
        listenToAuthConnect: listenToAuth,
        listenToProfileConnect: listenToProfile,
    },
)(ProfileViewContainer);

export default ProfileViewConnected;
