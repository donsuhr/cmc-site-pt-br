import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchAnalysts } from './actions';
import { chooseAnalyst } from '../SupportConsultationsSignup2019/actions';
import ChooseAnalyst from './ChooseAnalyst';

export class ChooseAnalystContainer extends React.Component {
    static propTypes = {
        // eslint-disable-next-line react/forbid-prop-types
        analysts: PropTypes.object,
        // eslint-disable-next-line react/forbid-prop-types
        user: PropTypes.object,
        chooseAnalystConnect: PropTypes.func,
        fetchAnalystsConnect: PropTypes.func,
        confirmedProfile: PropTypes.bool,
        // eslint-disable-next-line react/forbid-prop-types
        history: PropTypes.object,
    };

    componentWillMount() {
        const { confirmedProfile, history } = this.props;
        if (!confirmedProfile) {
            history.replace('/verify-registration');
        }
    }

    componentDidMount() {
        const {
            analysts, fetchAnalystsConnect, user, history,
        } = this.props;
        if (analysts.hasEverLoaded === false && analysts.fetching === false) {
            fetchAnalystsConnect();
        }
        setTimeout(() => {
            if (!user.isAuth) {
                history.replace('/');
            }
        }, 1000);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const { user, history } = this.props;
        if (prevProps.user.isAuth !== user.isAuth && user.isAuth === false) {
            history.replace('/');
        }
    }

    chooseAnalyst = (id) => {
        const { chooseAnalystConnect, history } = this.props;
        chooseAnalystConnect(id);
        history.push('/choose-analyst-time');
    };

    render() {
        const { analysts } = this.props;
        return (
            <div>
                <ChooseAnalyst
                    analysts={analysts}
                    chooseAnalyst={this.chooseAnalyst}
                />
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        analysts: state.analysts,
        user: state.user,
        confirmedProfile: state.ui.confirmedProfile,
        history: ownProps.history,
    };
}

const ChooseAnalystConnected = connect(
    mapStateToProps,
    {
        fetchAnalystsConnect: fetchAnalysts,
        chooseAnalystConnect: chooseAnalyst,
    },
)(ChooseAnalystContainer);

export default ChooseAnalystConnected;
