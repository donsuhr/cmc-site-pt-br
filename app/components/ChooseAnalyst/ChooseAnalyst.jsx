import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ShowError from '../ShowError';
import CallResizeIframe from '../CallResizeIframe';

const ChooseAnalyst = ({ analysts, chooseAnalyst }) => {
    const showLoadError = analysts.loadStatus
        && analysts.loadStatus !== 'success'
        && analysts.loadStatus !== '';
    return (
        <div className="support-consultations__select-analyst">
            <h2 className="ht--s--18">Select An Analyst</h2>
            <p>Now choose an analyst to meet with.</p>
            {showLoadError && <ShowError error={analysts.loadStatus} />}
            <ul className="select-analyst-list">
                {Object.keys(analysts.byId).map((key) => {
                    const analyst = analysts.byId[key];
                    return (
                        <li
                            key={analyst._id}
                            className="select-analyst-list-item"
                        >
                            <button
                                className="select-analyst-button"
                                type="button"
                                onClick={(event) => {
                                    event.preventDefault();
                                    chooseAnalyst(analyst._id);
                                }}
                            >
                                <div className="select-analyst-list-item__name">
                                    {analyst.firstName}
                                    {' '}
                                    {analyst.lastName}
                                </div>
                                <div className="select-analyst-list-item__topic">
                                    {analyst.topic}
                                </div>
                            </button>
                        </li>
                    );
                })}
            </ul>
            <Link
                className="pill-button mt1 cmc-article__link--reversed pill-button--reversed"
                to="/verify-registration"
            >
                Back
            </Link>

            <CallResizeIframe />
        </div>
    );
};

ChooseAnalyst.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    analysts: PropTypes.object,
    chooseAnalyst: PropTypes.func,
};

export default ChooseAnalyst;
