import React from 'react';
import { getDataFromFile } from './xlsx-file-reader';
import Results from './Results.container';

class ImportView extends React.Component {
    static propTypes = {};

    constructor() {
        super();
        this.state = {
            result: [],
        };
    }

    componentDidMount() {
        document.body.addEventListener('dragenter', this.handleDragover, false);
        document.body.addEventListener('dragover', this.handleDragover, false);
        document.body.addEventListener('drop', this.handleDrop, false);
    }

    componentWillUnmount() {
        document.body.removeEventListener(
            'dragenter',
            this.handleDragover,
            false,
        );
        document.body.removeEventListener(
            'dragover',
            this.handleDragover,
            false,
        );
        document.body.removeEventListener('drop', this.handleDrop, false);
    }

    handleDragover = (e) => {
        e.stopPropagation();
        e.preventDefault();
        e.dataTransfer.dropEffect = 'copy';
    };

    handleDrop = (e) => {
        e.stopPropagation();
        e.preventDefault();
        getDataFromFile(e.dataTransfer.files[0]).then((result) => {
            this.setState({ result });
        });
    };

    render() {
        const { result } = this.state;
        return (
            <div>
                <p className="import-view__instruction">Drop a spreadsheet file anywhere to start</p>
                <Results json={result} />
            </div>
        );
    }
}

export default ImportView;
