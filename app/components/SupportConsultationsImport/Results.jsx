/* eslint no-underscore-dangle: [2, { "allow": ["__rowNum__"] }] */

import React from 'react';
import PropTypes from 'prop-types';
import moment from '../util/load-moment-tz';

class Results extends React.Component {
    static propTypes = {
        // eslint-disable-next-line react/forbid-prop-types
        json: PropTypes.object,
        removeEasyBook: PropTypes.func,
        easyBook: PropTypes.func,
        getAnalystByName: PropTypes.func,
        getAnalystSlotByTime: PropTypes.func,
        getSlotDetails: PropTypes.func,
        getEasyBookById: PropTypes.func,
    };

    addSlot(availabilityId, row, easybookItem) {
        const { removeEasyBook, easyBook } = this.props;
        if (easybookItem) {
            removeEasyBook(easybookItem).then(() => {
                easyBook({
                    availabilityId,
                    firstName: row.FIRST_NAME,
                    lastName: row.LAST_NAME,
                    institution: row.COMPANY_NAME,
                });
            });
        } else {
            easyBook({
                availabilityId,
                firstName: row.FIRST_NAME,
                lastName: row.LAST_NAME,
                institution: row.COMPANY_NAME,
            });
        }
    }

    render() {
        const {
            json, getAnalystByName, getAnalystSlotByTime, getSlotDetails, getEasyBookById,
        } = this.props;
        return (
            <div>
                <table className="support-consultations-import__table">
                    <thead>
                        <tr>
                            <th>Client</th>
                            <th>Analyst</th>
                            <th>Time</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {json.map((row) => {
                            const analyst = row.SESSION_TITLE.replace(
                                'Support Consultation Appointment with ',
                                '',
                            );
                            const analystFn = analyst.split(' ')[0];
                            const analystLn = analyst
                                .split(' ')
                                .splice(1)
                                .join(' ');
                            const time = moment.tz(
                                `${row.START_DATE} ${row.START_TIME}`,
                                'YYYY-MM-DD h:mmA',
                                'America/New_York',
                            );

                            const matchAnalyst = getAnalystByName(
                                analystFn,
                                analystLn,
                            );
                            const foundMatchingAnalyst = !!matchAnalyst;
                            let status = '';
                            let availablityId;
                            let easybookItem;
                            const userAptCount = json.reduce(
                                (acc, x) => {
                                    if (
                                        row.FIRST_NAME === x.FIRST_NAME
                                        && row.LAST_NAME === x.LAST_NAME
                                    ) {
                                        acc += 1;
                                    }
                                    return acc;
                                },
                                0,
                            );
                            if (foundMatchingAnalyst) {
                                availablityId = getAnalystSlotByTime(
                                    matchAnalyst,
                                    time,
                                );
                                if (availablityId) {
                                    const slot = getSlotDetails(
                                        availablityId,
                                    );
                                    const slotDetails = slot && slot.val;
                                    if (slotDetails) {
                                        easybookItem = getEasyBookById(
                                            availablityId,
                                        );
                                        if (
                                            slotDetails.firstName
                                            === row.FIRST_NAME
                                            && slotDetails.lastName
                                            === row.LAST_NAME
                                            && slotDetails.institution
                                            === row.COMPANY_NAME
                                        ) {
                                            status = 'matched';
                                        } else {
                                            status = 'conflict';
                                        }
                                    }
                                }
                            } else {
                                status = 'No Matching Analyst';
                            }

                            return (
                                <tr key={row.__rowNum__}>
                                    <td>
                                        {row.FIRST_NAME}
                                        {' '}
                                        {row.LAST_NAME}
                                    </td>
                                    <td>
                                        {analystFn}
                                        {' '}
                                        {analystLn}
                                    </td>
                                    <td>
                                        {time.format(
                                            'MMM DD hh:mm A',
                                        )}
                                    </td>
                                    <td>
                                        {status}
                                        {' '}
                                        {userAptCount > 1 && (
                                            <div className="double-booker-notice">
                                                Double
                                                Booked
                                                (
                                                {' '}
                                                {
                                                    userAptCount
                                                }
                                                {' '}
                                                )
                                            </div>
                                        )}
                                    </td>
                                    <td>
                                        {(status === 'conflict' || status === '') && (
                                            <button
                                                type="button"
                                                onClick={() => {
                                                    this.addSlot(availablityId, row, easybookItem);
                                                }}
                                                className="support-con-import__button"
                                            >
                                                {status === 'conflict' ? 'Replace' : 'Add'}
                                            </button>
                                        )}
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Results;

/*
var example = {
    ATTENDEE_TITLE: 'Senior Business Analyst',
    COMPANY_NAME: 'Collegis Education',
    'CORPORATE NAME / PARENT COMPANY': '',
    EMAIL: 'Jeffrey.Claseman@Collegiseducation.com',
    FIRST_NAME: 'Jeffrey',
    LAST_NAME: 'Claseman',
    SALES_DATE: '02/27/2018 11:55 AM',
    SESSION_TITLE: 'Support Consultation Appointment with Alex Del Vecchio',
    SESSION_TYPE: 'Financial Aid / FormsBuilder / WorkFlow',
    START_DATE: '2018-04-18',
    START_TIME: '3:00PM',
    __rowNum__: 1,
};
*/
