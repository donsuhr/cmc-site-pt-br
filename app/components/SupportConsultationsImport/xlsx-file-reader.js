import XLSX from 'xlsx';

const rABS = true;

function getDataFromFile(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();

        reader.onload = (e) => {
            try {
                let data = e.target.result;
                if (!rABS) {
                    data = new Uint8Array(data);
                }
                const workbook = XLSX.read(data, {
                    type: rABS ? 'binary' : 'array',
                });

                const xlsJson = XLSX.utils.sheet_to_json(
                    workbook.Sheets[workbook.SheetNames[0]],
                );
                resolve(xlsJson);

                /* DO SOMETHING WITH workbook HERE */
            } catch (error) {
                reject(e);
            }
        };

        reader.onerror = (e) => {
            reject(e);
        };

        if (rABS) {
            reader.readAsBinaryString(file);
        } else {
            reader.readAsArrayBuffer(file);
        }
    });
}

export { getDataFromFile };
