import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import View from './View';

const App = ({ store, ...props }) => (
    <Provider store={store}>
        <View />
    </Provider>
);

App.propTypes = {
    /* eslint-disable react/forbid-prop-types */
    store: PropTypes.object.isRequired,
};

export default App;
