import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Results from './Results';
import { fetchAnalysts } from '../ChooseAnalyst/actions';
import {
    uniqueStartTimes,
    getAnalystByName,
    getAnalystSlotByTime,
} from '../ChooseAnalyst/reducer';
import { listenToAuth } from '../firebase/auth.actions';
import { listenToUsedAvailability } from '../firebase/schedule.support.actions';
import {
    listenToEasyBook,
    easyBook,
    removeEasyBook,
} from '../firebase/easybook.actions';
import { getItemById } from '../firebase/easybook.reducer';
import { getRegularBookedByAvailabilityId } from '../firebase/schedule.support.reducer';

import Loading from '../Loading';

export class GridContainer extends React.Component {
    static propTypes = {
        analysts: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        // eslint-disable-next-line react/forbid-prop-types
        uniqueStartTimesConnect: PropTypes.array,
        supportConsultations: PropTypes.shape({
            hasEverLoadedUsedAvailability: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        easybookState: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        // eslint-disable-next-line react/forbid-prop-types
        json: PropTypes.object,
        fetchAnalystsConnect: PropTypes.func,
        listenToUsedAvailabilityConnect: PropTypes.func,
        listenToEasyBookConnect: PropTypes.func,
        listenToAuthConnect: PropTypes.func,
        easyBookConnect: PropTypes.func,
        getAnalystByNameConnect: PropTypes.func,
        getAnalystSlotByTimeConnect: PropTypes.func,
        getSlotDetails: PropTypes.func,
        getEasyBookById: PropTypes.func,
        removeEasyBookConnect: PropTypes.func,
    };

    componentDidMount() {
        const {
            analysts,
            listenToAuthConnect,
            fetchAnalystsConnect,
            listenToUsedAvailabilityConnect,
            listenToEasyBookConnect,
        } = this.props;
        if (analysts.hasEverLoaded === false && analysts.fetching === false) {
            fetchAnalystsConnect();
        }
        listenToAuthConnect().then((user) => {
            listenToUsedAvailabilityConnect();
            listenToEasyBookConnect();
        });
    }

    render() {
        const {
            supportConsultations,
            easybookState,
            easyBookConnect,
            analysts,
            uniqueStartTimesConnect,
            json,
            getAnalystByNameConnect,
            getAnalystSlotByTimeConnect,
            getSlotDetails,
            getEasyBookById,
            removeEasyBookConnect,
        } = this.props;
        if (
            !supportConsultations.hasEverLoadedUsedAvailability
            || !easybookState.hasEverLoaded
        ) {
            return <Loading>Loading Data...</Loading>;
        }
        if (analysts.hasEverLoaded === false || analysts.fetching) {
            return <Loading>Loading Analysts...</Loading>;
        }

        return (
            <Results
                analysts={analysts}
                uniqueStartTimes={uniqueStartTimesConnect}
                json={json}
                getAnalystByName={getAnalystByNameConnect}
                getAnalystSlotByTime={getAnalystSlotByTimeConnect}
                getSlotDetails={getSlotDetails}
                easyBook={easyBookConnect}
                getEasyBookById={getEasyBookById}
                removeEasyBook={removeEasyBookConnect}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        analysts: state.analysts,
        easybookState: state.easybook,
        supportConsultations: state.supportConsultations,
        uniqueStartTimesConnect: uniqueStartTimes(state.analysts),
        json: ownProps.json,
        getAnalystByNameConnect: getAnalystByName.bind(null, state.analysts),
        getAnalystSlotByTimeConnect: getAnalystSlotByTime,
        getEasyBookById: getItemById.bind(null, state.easybook),
        getSlotDetails: (availabilityId) => {
            let userProfile;
            const regularBooked = getRegularBookedByAvailabilityId(
                state.supportConsultations,
                availabilityId,
            );
            const userId = regularBooked ? regularBooked.userId : undefined;
            if (userId && userId !== 'easybook') {
                userProfile = state.userProfiles.byId[userId];
            }
            if (userId && userId === 'easybook') {
                if (!state.easybook.hasEverLoaded || state.easybook.fetching) {
                    userProfile = {
                        val: {
                            firstName: 'loading...',
                        },
                    };
                } else {
                    const easybookItem = state.easybook.byAvailabilityId[availabilityId];
                    userProfile = {
                        val: easybookItem && easybookItem.val.info,
                    };
                }
            }
            return userProfile;
        },
    };
}

const GridConnected = connect(
    mapStateToProps,
    {
        fetchAnalystsConnect: fetchAnalysts,
        listenToAuthConnect: listenToAuth,
        listenToUsedAvailabilityConnect: listenToUsedAvailability,
        listenToEasyBookConnect: listenToEasyBook,
        easyBookConnect: easyBook,
        removeEasyBookConnect: removeEasyBook,
    },
)(GridContainer);

export default GridConnected;
