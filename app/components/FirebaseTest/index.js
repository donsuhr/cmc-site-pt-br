import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/functions';

function showLoading() {
    // <div class="loading--placeholder">Loading...</div>
    const div = document.createElement('div');
    div.setAttribute('class', 'loading--placeholder');
    div.innerText = 'Loading...';
    document.querySelector('#CiAppMountPoint').appendChild(div);
}

function hideLoading() {
    document.querySelectorAll('.loading--placeholder').forEach((x) => {
        x.parentNode.removeChild(x);
    });
}

async function updateDbText(textOrDb) {
    if (typeof textOrDb === 'string') {
        document.querySelector('.db').innerText = textOrDb;
    } else {
        const dbRef = await textOrDb.ref('/').once('value');
        const dbVal = JSON.stringify(dbRef.val(), null, '  ');
        document.querySelector('.db').innerText = dbVal;
    }
}

function reloadClickedBound(db) {
    return async (event) => {
        event.preventDefault();
        showLoading();
        await updateDbText(db);
        hideLoading();
    };
}

async function resetDb(db) {
    await db.ref('/').set({});
    await updateDbText(db);
}

function resetClickedBound(db) {
    return async (event) => {
        event.preventDefault();
        showLoading();
        await resetDb(db);
        hideLoading();
    };
}

async function addUser(db) {
    await db
        .ref('/users')
        .update({ userid1: { profile: { email: 'test1@donsuhr.com' } } });
    await updateDbText(db);
}

function addUserBound(db) {
    return async (event) => {
        event.preventDefault();
        showLoading();
        await addUser(db);
        hideLoading();
    };
}

async function addUserAppt1(db) {
    const availabilityId = `availId-${Math.floor(Math.random() * 100)}`;
    const { key } = await db
        .ref('/users/userid1/schedule/supportCouncil')
        .push({
            analystId: 'analystId1',
            availabilityId,
            start: '2020-04-29T19:00:00.000Z',
            end: '2020-04-29T19:30:00.000Z',
        });
    await db.ref(`/usedAvail/userid1/${availabilityId}`).set(key);
    await updateDbText(db);
}

function addUserApt1Bound(db) {
    return async (event) => {
        event.preventDefault();
        showLoading();
        await addUserAppt1(db);
        hideLoading();
    };
}

async function removeUserAppt(db) {
    const apptRef = await db
        .ref('/users/userid1/schedule/supportCouncil')
        .once('value');
    const appts = apptRef.val();
    const firstApptKey = Object.keys(appts)[0];
    console.log('first appointm', firstApptKey);
    const { availabilityId } = appts[firstApptKey];
    console.log('avail id', availabilityId);
    const firstAppRef = await db
        .ref(`/users/userid1/schedule/supportCouncil/${firstApptKey}`)
        .set(null);
    await db.ref(`/usedAvail/userid1/${availabilityId}`).set(null);
}

function delUserApt1Bound(db) {
    return async (event) => {
        event.preventDefault();
        showLoading();
        await removeUserAppt(db);
        await updateDbText(db);
        hideLoading();
    };
}

async function hydrateDb(db) {
    const usedAvailRef = db.ref('usedAvail');
    const snapshot = await usedAvailRef.once('value');
    console.log('snap', snapshot.val());
    const vals = snapshot.val();
    if (!vals || (vals && vals.length < 1)) {
        const appt1FirebasePushId = '123';
        const appt2FirebasePushId = '223';
        const appt3FirebasePushId = '323';
        const update2 = await db.ref('users').update({
            userId1: {
                profile: {
                    email: 'userId1@email.com',
                },
                schedule: {
                    supportCouncil: {
                        [appt1FirebasePushId]: {
                            availabilityId: 'availId123',
                            analystId: 'analyst123',
                            start: '2020-04-29T19:00:00.000Z',
                            end: '2020-04-29T19:30:00.000Z',
                        },
                        423: {
                            availabilityId: 'availId133',
                            analystId: 'analyst123',
                            start: '2020-04-29T19:30:00.000Z',
                            end: '2020-04-29T20:00:00.000Z',
                        },
                    },
                },
            },
            userId2: {
                schedule: {
                    supportConsul: {
                        [appt2FirebasePushId]: { availabilityId: 'availId123' },
                    },
                },
            },
            userId3: {
                schedule: {
                    supportConsul: {
                        [appt3FirebasePushId]: { availabilityId: 'availId123' },
                    },
                },
            },
        });
        return usedAvailRef.update({
            userId1: { availId123: appt1FirebasePushId, availId133: '423' },
            userId2: { availId234: appt2FirebasePushId },
            userId3: { availId334: appt3FirebasePushId },
        });
    }
    console.log('skipping db write');
    return Promise.resolve();
}

async function logAppointments(userId) {
    const emailRef = await app
        .database()
        .ref('/users/userId1/profile/email')
        .once('value');
    const email = emailRef.val();

    const userScheduleRef = await db
        .ref('/users/userId1/schedule/supportCouncil')
        .once('value');
    const scheduleRef = await db.ref(`usedAvail/${userId}`).once('value');
    const appointments = await Promise.all(
        Object.entries(scheduleRef.val()).map(
            ([availabilityId, schedulePushId]) => app
                .database()
                .ref(
                    `/users/${userId}/schedule/supportCouncil/${schedulePushId}`,
                )
                .once('value'),
        ),
    );
    appointments.forEach((ref) => {
        const { start, end } = ref.val();
        console.log(`user (${email}) has item from ${start} to ${end}`);
    });
}

async function config({ year }) {
    showLoading();
    updateDbText('Loading...');
    const instance = 'ci-app-2020';
    const app = firebase.initializeApp(
        {
            databaseURL: 'http://localhost:5002?ns=campusinsight-2020',
        },
        instance,
    );
    app.functions().useFunctionsEmulator('http://localhost:5003');

    const db = app.database();
    updateDbText(db);

    document
        .querySelector('.resetDbBtn')
        .addEventListener('click', resetClickedBound(db));
    document
        .querySelector('.addUserBtn')
        .addEventListener('click', addUserBound(db));
    document
        .querySelector('.addUserAppt1Btn')
        .addEventListener('click', addUserApt1Bound(db));
    document
        .querySelector('.delUserAppt1Btn')
        .addEventListener('click', delUserApt1Bound(db));
    document
        .querySelector('.reloadBtn')
        .addEventListener('click', reloadClickedBound(db));
    hideLoading();
}

const api = {
    config,
};

export default api;
