import React from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import { Form } from 'formsy-react';
import MyInput from './Input';
import Loading from '../Loading';
import createFirebaseApp from '../../scripts/firebase';

class SignupForm extends React.Component {
    static propTypes = {
        setMode: PropTypes.func,
        year: PropTypes.string,
    };

    state = {
        canSubmit: false,
        signinError: '',
        loading: false,
    };

    componentDidMount() {
        $('.account-modal .modal-title').text('Create Account');
        $('.account-modal [name="email"]').trigger('focus');
    }

    submit = (data) => {
        this.setState({
            loading: true,
        });
        const { year, setMode } = this.props;
        const firebaseAuth = createFirebaseApp(year).auth;
        firebaseAuth
            .createUserWithEmailAndPassword(data.email, data.password)
            .then(() => {
                setMode('loginsuccess');
                window.location.href = '/my-conference/';
            })
            .catch((error) => {
                this.setState({
                    signinError: error.message,
                    loading: false,
                });
            });
    };

    enableButton = () => {
        this.setState({ canSubmit: true });
    };

    disableButton = () => {
        this.setState({ canSubmit: false });
    };

    render() {
        const { setMode } = this.props;
        const { signinError, canSubmit, loading } = this.state;
        return (
            <Form
                className="cmc-form cmc-form--centered cmc-form--login"
                onSubmit={this.submit}
                onValid={this.enableButton}
                onInvalid={this.disableButton}
            >
                <p>To create an account, complete the form below.</p>
                <p className="signin-error">{signinError}</p>
                {loading && <Loading>Loading...</Loading>}
                <fieldset>
                    <ul>
                        <li>
                            <MyInput
                                name="email"
                                title="Email"
                                validations="isEmail"
                                validationError="This is not a valid email"
                                required
                            />
                        </li>
                        <li>
                            <MyInput
                                name="password"
                                type="password"
                                title="Password"
                                validations="minLength:6"
                                validationError="Password should be at least 6 characters"
                                required
                            />
                            <p className="input-note">6 character minimum</p>
                        </li>
                        <li>
                            <MyInput
                                name="confirmPassword"
                                title="Confirm Password"
                                type="password"
                                validations="equalsField:password"
                                validationError="Passwords do not match"
                                required
                            />
                        </li>
                        <li className="cmc-form__submit-row cmc-form__submit-row--signup">
                            <button
                                className="cmc-form__submit-button pill-button"
                                type="submit"
                                disabled={!canSubmit}
                            >
                                Submit
                            </button>
                            <button
                                type="button"
                                className="cmc-form__submit-button cmc-form__submit-button--back pill-button"
                                onClick={(event) => {
                                    event.preventDefault();
                                    setMode('login');
                                }}
                            >
                                Back
                            </button>
                        </li>
                    </ul>
                </fieldset>
            </Form>
        );
    }
}

export default SignupForm;
