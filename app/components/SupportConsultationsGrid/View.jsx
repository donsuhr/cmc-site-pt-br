import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    listenToAuth,
    requestPopupAuth,
    listenToUserRoot,
} from '../firebase/auth.actions';
import { PropType as userPropType } from '../firebase/auth.reducer';
import Grid from './Grid.container';
import Loading from '../Loading';

export class ScheduleGridView extends React.Component {
    static propTypes = {
        listenToAuthConnect: PropTypes.func.isRequired,
        listenToUserRootConnect: PropTypes.func.isRequired,
        user: userPropType,
        year: PropTypes.string,
    };

    componentDidMount() {
        const { listenToAuthConnect, listenToUserRootConnect } = this.props;
        listenToAuthConnect().then(user => {
            listenToUserRootConnect(user.uid);
        });
    }

    onSignInClick = event => {
        const { year } = this.props;
        event.preventDefault();
        requestPopupAuth(year);
    };

    render() {
        const { user } = this.props;
        if (user.initting) {
            return <Loading>Loading...</Loading>;
        }
        if (user.loadingRoot) {
            return <Loading>Authenticating...</Loading>;
        }
        if (!user.isAuth) {
            return (
                <button
                    type="button"
                    className="cmc-article__link pill-button"
                    onClick={this.onSignInClick}
                >
                    Sign In
                </button>
            );
        }
        if (!user.isAdmin) {
            return <p>Admin rights required.</p>;
        }
        return (
            <div>
                <Grid />
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user,
        year: state.domAttributes.year,
    };
}

const ScheduleGridViewConnected = connect(mapStateToProps, {
    listenToAuthConnect: listenToAuth,
    listenToUserRootConnect: listenToUserRoot,
})(ScheduleGridView);

export default ScheduleGridViewConnected;
