import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Grid from './Grid';
import { fetchAnalysts } from '../ChooseAnalyst/actions';
import { uniqueStartTimes } from '../ChooseAnalyst/reducer';
import { listenToAuth } from '../firebase/auth.actions';
import { listenToUsedAvailability } from '../firebase/schedule.support.actions';
import { listenToEasyBook } from '../firebase/easybook.actions';
import Loading from '../Loading';

export class GridContainer extends React.Component {
    static propTypes = {
        analysts: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        // eslint-disable-next-line react/forbid-prop-types
        uniqueStartTimesConnect: PropTypes.array,
        supportConsultations: PropTypes.shape({
            hasEverLoadedUsedAvailability: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        easybookState: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        fetchAnalystsConnect: PropTypes.func,
        listenToUsedAvailabilityConnect: PropTypes.func,
        listenToAuthConnect: PropTypes.func,
        listenToEasyBookConnect: PropTypes.func,
    };

    componentDidMount() {
        const {
            analysts,
            fetchAnalystsConnect,
            listenToAuthConnect,
            listenToUsedAvailabilityConnect,
            listenToEasyBookConnect,
        } = this.props;
        if (analysts.hasEverLoaded === false && analysts.fetching === false) {
            fetchAnalystsConnect();
        }
        listenToAuthConnect().then((user) => {
            listenToUsedAvailabilityConnect();
            listenToEasyBookConnect();
        });
    }

    render() {
        const {
            supportConsultations,
            easybookState,
            analysts,
            uniqueStartTimesConnect,
        } = this.props;
        if (
            !supportConsultations.hasEverLoadedUsedAvailability
            || !easybookState.hasEverLoaded
        ) {
            return <Loading>Loading Data...</Loading>;
        }
        if (analysts.hasEverLoaded === false || analysts.fetching) {
            return <Loading>Loading Analysts...</Loading>;
        }

        return <Grid analysts={analysts} uniqueStartTimes={uniqueStartTimesConnect} />;
    }
}

function mapStateToProps(state, ownProps) {
    return {
        analysts: state.analysts,
        easybookState: state.easybook,
        supportConsultations: state.supportConsultations,
        uniqueStartTimesConnect: uniqueStartTimes(state.analysts),
    };
}

const GridConnected = connect(
    mapStateToProps,
    {
        fetchAnalystsConnect: fetchAnalysts,
        listenToAuthConnect: listenToAuth,
        listenToUsedAvailabilityConnect: listenToUsedAvailability,
        listenToEasyBookConnect: listenToEasyBook,
    },
)(GridContainer);

export default GridConnected;
