/* eslint-disable react/forbid-prop-types */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { reset } from 'redux-form';
import queryString from 'query-string';
import VerifyRegistration from './VerifyRegistration';
import { confirmProfile } from '../SupportConsultationsSignup2019/actions';
import { listenToAuth } from '../firebase/auth.actions';
import { updateProfile, listenToProfile } from '../firebase/profile.actions';
import { listenToScheduleSupport } from '../firebase/schedule.support.actions';

export class VerifyRegistrationContainer extends React.Component {
    static propTypes = {
        user: PropTypes.object,
        profile: PropTypes.object,
        supportConsultations: PropTypes.object,
        listenToAuthConnect: PropTypes.func,
        listenToProfileConnect: PropTypes.func,
        listenToScheduleSupportConnect: PropTypes.func,
        updateProfileConnect: PropTypes.func,
        confirmProfileConnect: PropTypes.func,
        resetConnect: PropTypes.func,
        queryAllow: PropTypes.string,
        history: PropTypes.object,
    };

    componentDidMount() {
        this.listenForLogin();
        const { user, history } = this.props;
        setTimeout(() => {
            if (!user.isAuth) {
                history.replace('/');
            }
        }, 1000);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const { user, history } = this.props;
        if (prevProps.user.isAuth !== user.isAuth && user.isAuth === false) {
            this.listenForLogin();
            history.replace('/');
        }
    }

    listenForLogin = () => {
        const {
            listenToAuthConnect,
            listenToScheduleSupportConnect,
            profile,
            listenToProfileConnect,
        } = this.props;
        listenToAuthConnect().then((user) => {
            listenToScheduleSupportConnect(user.uid);
            if (!profile.fetching && profile.loadStatus !== 'success') {
                listenToProfileConnect(user);
            }
        });
    };

    handleSubmit = (data) => {
        const { updateProfileConnect, user } = this.props;
        return updateProfileConnect(user.info.uid, data);
    };

    handleSubmitSuccess = () => {
        const { resetConnect, confirmProfileConnect, history } = this.props;
        resetConnect('RegistrationForm');
        confirmProfileConnect();
        history.push('/choose-analyst');
    };

    render() {
        const {
            supportConsultations, queryAllow, user, profile,
        } = this.props;
        if (
            supportConsultations.userItems.length > 0
            && queryAllow !== '1233'
        ) {
            return (
                <div>
                    <p>
                        You may only sign-up for one appointment at this time.
                        To sign up for more, please visit the Support
                        Consultation Desk.
                    </p>
                    <Link
                        className="cmc-article__link cmc-article__link--reversed"
                        to="/"
                    >
                        Back
                    </Link>
                </div>
            );
        }
        return (
            <VerifyRegistration
                user={user}
                profile={profile}
                handleSubmit={this.handleSubmit}
                handleSubmitSuccess={this.handleSubmitSuccess}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    const parsed = queryString.parse(window.location.search);
    return {
        profile: state.profile,
        user: state.user,
        supportConsultations: state.supportConsultations,
        queryAllow: parsed.allow,
        history: ownProps.history,
    };
}

const VerifyRegistrationConnected = connect(
    mapStateToProps,
    {
        listenToAuthConnect: listenToAuth,
        listenToProfileConnect: listenToProfile,
        updateProfileConnect: updateProfile,
        confirmProfileConnect: confirmProfile,
        listenToScheduleSupportConnect: listenToScheduleSupport,
        resetConnect: reset,
    },
)(VerifyRegistrationContainer);

export default VerifyRegistrationConnected;
