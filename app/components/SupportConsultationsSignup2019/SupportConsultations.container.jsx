import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import SupportConsultations from './SupportConsultations';
import { fetchAnalysts } from '../ChooseAnalyst/actions';
import { listenToAuth, genericEmailLogin } from '../firebase/auth.actions';
import { PropType as userPropType } from '../firebase/auth.reducer';
import {
    listenToScheduleSupport,
    removeSupportFromSchedule,
} from '../firebase/schedule.support.actions';
import {
    getAppointmentsByDay,
    getScheduleItemById,
} from '../firebase/schedule.support.reducer';
import Loading from '../Loading';
import CallResizeIframe from '../CallResizeIframe';

export class SupportConsultationsContainer extends React.Component {
    static propTypes = {
        // eslint-disable-next-line react/forbid-prop-types
        analysts: PropTypes.object,
        // eslint-disable-next-line react/forbid-prop-types
        supportConsultations: PropTypes.object,
        // eslint-disable-next-line react/forbid-prop-types
        userItemsByDay: PropTypes.object,
        user: userPropType,
        fetchAnalystsConnect: PropTypes.func,
        listenToAuthConnect: PropTypes.func,
        listenToScheduleSupportConnect: PropTypes.func,
        getScheduleItemByIdConnect: PropTypes.func,
        genericEmailLoginConnect: PropTypes.func,
        removeSupportFromScheduleConnect: PropTypes.func,
    };

    state = {
        signinError: '',
        inputValue: '',
        didCancelAppt: false,
    };

    componentDidMount() {
        const { analysts, fetchAnalystsConnect } = this.props;
        if (analysts.hasEverLoaded === false && analysts.fetching === false) {
            fetchAnalystsConnect();
        }
        // logout doesnt unmount this component, so it never listens for the schedule again
        this.listenForLogin();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const { user } = this.props;
        if (prevProps.user.isAuth !== user.isAuth && user.isAuth === false) {
            this.listenForLogin();
        }
    }

    onEmailSubmit = (event) => {
        event.preventDefault();
        const { genericEmailLoginConnect } = this.props;
        const result = genericEmailLoginConnect(this.email.value);
        result.then((error) => {
            this.setState({
                signinError: error,
            });
        });
    };

    onInputChange = (event) => {
        this.setState({ inputValue: event.target.value });
    };

    onKeyUp = (event) => {
        if (event.key === 'Enter') {
            this.setState({ inputValue: event.target.value });
            this.onEmailSubmit(event);
        }
    };

    listenForLogin = () => {
        const {
            listenToAuthConnect,
            listenToScheduleSupportConnect,
        } = this.props;
        listenToAuthConnect().then((user) => {
            listenToScheduleSupportConnect(user.uid);
        });
    };

    removeItem = (item) => {
        const {
            getScheduleItemByIdConnect,
            removeSupportFromScheduleConnect,
        } = this.props;
        this.setState({ didCancelAppt: true });
        const scheduleItem = getScheduleItemByIdConnect(
            item.analystId,
            item.availabilityId,
        );
        removeSupportFromScheduleConnect(scheduleItem);
    };

    render() {
        const {
            user,
            analysts,
            supportConsultations,
            userItemsByDay,
        } = this.props;
        const { inputValue, signinError, didCancelAppt } = this.state;

        // @formatter:off
        const fetching = !analysts.hasEverLoaded
            || analysts.fetching
            || !supportConsultations.hasEverLoaded
            || supportConsultations.fetching;
        // @formatter:on

        if (
            !fetching
            && !user.initting
            && !didCancelAppt
            && Object.keys(userItemsByDay).length === 0
        ) {
            return <Redirect to="/verify-registration" />;
        }

        if (user.initting) {
            return <Loading>Loading...</Loading>;
        }

        if (!user.isAuth) {
            return (
                <div>
                    <p>
                        To schedule or change your appointment, type your email
                        address
                    </p>
                    <form
                        onSubmit={this.onEmailSubmit}
                        className="cmc-form cmc-form--registration-form"
                    >
                        <fieldset>
                            <ul>
                                <li>
                                    <div>
                                        <label htmlFor="email">Email</label>
                                        <div className="input-wrapper">
                                            <input
                                                type="text"
                                                ref={(node) => {
                                                    this.email = node;
                                                }}
                                                id="email"
                                                value={inputValue}
                                                onChange={this.onInputChange}
                                                onKeyUp={this.onKeyUp}
                                            />
                                        </div>
                                        <p className="signin-error">
                                            {signinError}
                                        </p>
                                    </div>
                                </li>
                                <li className="cmc-form__submit-row">
                                    <button
                                        className="pill-button cmc-form__submit-button"
                                        type="submit"
                                    >
                                        Next
                                    </button>
                                </li>
                            </ul>
                        </fieldset>
                    </form>
                    <CallResizeIframe/>
                </div>
            );
        }
        if (fetching) {
            return <Loading>Loading...</Loading>;
        }
        return (
            <SupportConsultations
                analysts={analysts}
                supportConsultations={supportConsultations}
                userItemsByDay={userItemsByDay}
                removeItem={this.removeItem}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user,
        analysts: state.analysts,
        supportConsultations: state.supportConsultations,
        userItemsByDay: getAppointmentsByDay(
            state.supportConsultations,
            state.analysts,
        ),
        getScheduleItemByIdConnect: getScheduleItemById.bind(
            null,
            state.supportConsultations,
        ),
        year: state.domAttributes.year,
    };
}

const SupportConsultationsConnected = connect(
    mapStateToProps,
    {
        fetchAnalystsConnect: fetchAnalysts,
        listenToAuthConnect: listenToAuth,
        removeSupportFromScheduleConnect: removeSupportFromSchedule,
        listenToScheduleSupportConnect: listenToScheduleSupport,
        genericEmailLoginConnect: genericEmailLogin,
    },
)(SupportConsultationsContainer);

export default SupportConsultationsConnected;
