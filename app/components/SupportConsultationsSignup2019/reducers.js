import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import analysts from '../ChooseAnalyst/reducer';
import supportConsultations from '../firebase/schedule.support.reducer';
import user from '../firebase/auth.reducer';
import profile from '../firebase/profile.reducer';
import listeners from '../firebase/listeners.reducer';
import ui from './reducer';
import domAttributes from '../redux/domAttributes.reducers';

export default combineReducers({
    user,
    analysts,
    profile,
    form: formReducer,
    ui,
    supportConsultations,
    listeners,
    domAttributes,
});
