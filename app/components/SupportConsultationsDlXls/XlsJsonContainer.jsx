import React from 'react';
import PropTypes from 'prop-types';
import saveAs from 'file-saver';
import isEqual from 'lodash/isEqual';
import connect from 'react-redux/es/connect/connect';
import config from 'config';
import { checkStatus } from 'fetch-json-helpers';
import { listenToEasyBook } from '../firebase/easybook.actions';
import { fetchAnalysts } from '../ChooseAnalyst/actions';
import { getAppointmentFromId } from '../ChooseAnalyst/reducer';
import { listenToAuth, listenToUserRoot } from '../firebase/auth.actions';
import { listenToUsedAvailability } from '../firebase/schedule.support.actions';
import { fetchUserProfile } from '../firebase/userProfiles.actions';
import Loading from '../Loading';

class XlsJsonContainer extends React.Component {
    static propTypes = {
        analystsConnect: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        listenToAuthConnect: PropTypes.func,
        listenToUserRootConnect: PropTypes.func,
        listenToUsedAvailabilityConnect: PropTypes.func,
        listenToEasyBookConnect: PropTypes.func,
        fetchUserProfileConnect: PropTypes.func,
        getAppointmentFromIdConnect: PropTypes.func,
        // eslint-disable-next-line react/forbid-prop-types
        usedAvailabilityIds: PropTypes.object,
        userProfiles: PropTypes.shape({
            byId: PropTypes.object,
        }),
        profilesLoaded: PropTypes.bool,
        easyBookState: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        fetchAnalystsConnect: PropTypes.func,
        user: PropTypes.shape({
            initting: PropTypes.bool,
            loadingRoot: PropTypes.bool,
            isAuth: PropTypes.bool,
            isAdmin: PropTypes.bool,
        }),
        supportConsultations: PropTypes.shape({
            hasEverLoadedUsedAvailability: PropTypes.bool,
            fetchingUsedAvailability: PropTypes.bool,
        }),
    };

    state = {
        requesting: false,
        requestError: '',
    };

    componentDidMount() {
        const {
            listenToAuthConnect,
            listenToUserRootConnect,
            listenToUsedAvailabilityConnect,
            listenToEasyBookConnect,
            analystsConnect,
            fetchAnalystsConnect,
        } = this.props;
        listenToAuthConnect().then((user) => {
            listenToUserRootConnect(user.uid);
            listenToUsedAvailabilityConnect();
            listenToEasyBookConnect();
        });
        if (
            analystsConnect.hasEverLoaded === false
            && analystsConnect.fetching === false
        ) {
            fetchAnalystsConnect();
        }
    }

    componentWillReceiveProps(nextProps) {
        const {
            usedAvailabilityIds,
            fetchUserProfileConnect,
            userProfiles,
        } = this.props;
        if (
            !isEqual(
                Object.keys(nextProps.usedAvailabilityIds),
                Object.keys(usedAvailabilityIds),
            )
        ) {
            Object.keys(nextProps.usedAvailabilityIds).forEach((id) => {
                const { userId } = nextProps.usedAvailabilityIds[id];
                if (
                    userId !== 'easybook'
                    && typeof userProfiles.byId[userId] === 'undefined'
                ) {
                    fetchUserProfileConnect(
                        nextProps.usedAvailabilityIds[id].userId,
                    );
                }
            });
        }
    }

    mungedata() {
        const {
            usedAvailabilityIds,
            userProfiles,
            easyBookState,
            getAppointmentFromIdConnect,
        } = this.props;
        const profile = {
            'first name': '',
            'last name': '',
            institution: '',
            email: '',
            phone: '',
            time: '',
            analyst: '',
            topic: '',
            mode: '',
            message: '',
        };
        return Object.keys(usedAvailabilityIds).reduce((acc, availablityId) => {
            const { userId } = usedAvailabilityIds[availablityId];

            if (userId === 'easybook') {
                const easyBookProfile = easyBookState.byAvailabilityId[availablityId].val.info;
                if (easyBookProfile === 'closed') {
                    return acc;
                }
                acc[availablityId] = {
                    ...profile,
                    'first name': easyBookProfile.firstName,
                    'last name': easyBookProfile.lastName,
                    institution: easyBookProfile.institution,
                    mode: 'easybook',
                };
            } else {
                const regularProfile = userProfiles.byId[userId].val;
                acc[availablityId] = {
                    ...profile,
                    'first name': regularProfile.firstName,
                    'last name': regularProfile.lastName,
                    institution: regularProfile.institution,
                    email: regularProfile.email,
                    phone: regularProfile.phone,
                    mode: 'regularbook',
                    message: regularProfile.message,
                };
            }
            const {
                analystObj: {
                    firstName: analystFirstName,
                    lastName: analystLastName,
                    topic,
                },
                availabilityObj: { start },
            } = getAppointmentFromIdConnect(availablityId);
            acc[
                availablityId
            ].analyst = `${analystFirstName} ${analystLastName}`;
            acc[availablityId].topic = topic;
            acc[availablityId].time = start;
            return acc;
        }, {});
    }

    makeRequest() {
        this.setState({ requesting: true, requestError: '' });
        const endpoint = `${config.domains.api}/campusInsight/ci/consultation-xls`;
        fetch(endpoint, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.mungedata()),
        })
            .then(checkStatus)
            .then((response) => response.blob())
            .then((blob) => saveAs(blob, 'support-consulations-2019.xlsx'))
            .catch((error) => {
                // eslint-disable-next-line no-console
                console.log(error);
                this.setState({ requestError: 'Error generating XlS' });
            })
            .then(() => {
                this.setState({ requesting: false });
            });
    }

    render() {
        const {
            supportConsultations,
            user,
            easyBookState,
            profilesLoaded,
            analystsConnect,
        } = this.props;

        const { requesting, requestError } = this.state;

        if (user.initting) {
            return <Loading>Loading...</Loading>;
        }
        if (user.loadingRoot) {
            return <Loading>Authenticating...</Loading>;
        }
        if (!user.isAuth) {
            return <p>Auth required</p>;
        }
        if (!user.isAdmin) {
            return <p>Admin rights required.</p>;
        }

        if (!analystsConnect.hasEverLoaded || analystsConnect.fetching) {
            return <Loading>Loading Analysts...</Loading>;
        }

        if (
            !supportConsultations.hasEverLoadedUsedAvailability
            && supportConsultations.fetchingUsedAvailability
        ) {
            return <Loading>Loading Availability...</Loading>;
        }

        if (!easyBookState.hasEverLoaded && easyBookState.fetching) {
            return <Loading>Loading Easybook...</Loading>;
        }

        if (!profilesLoaded) {
            return <Loading>Loading Profiles...</Loading>;
        }

        if (requesting) {
            return <Loading>Generating XLS...</Loading>;
        }

        return (
            <div>
                <button
                    type="button"
                    className="xlsJson__dl-btn pill-button"
                    onClick={this.makeRequest.bind(this)}
                >
                    Download XLS
                </button>
                {requestError && <p>{requestError}</p>}
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    const usedAvailabilityIds = Object.keys(
        state.supportConsultations.usedAvailabilityId,
    );
    const profilesLoaded = usedAvailabilityIds.length > 0
        && usedAvailabilityIds.every((x) => {
            const { userId } = state.supportConsultations.usedAvailabilityId[x];
            const isEasyBook = userId === 'easybook';
            const hasProfileKey = Object.hasOwnProperty.call(
                state.userProfiles.byId,
                userId,
            );
            const isLoading = hasProfileKey && !state.userProfiles.byId[userId].loading;
            return isEasyBook || isLoading;
        });

    return {
        user: state.user,
        supportConsultations: state.supportConsultations,
        userProfiles: state.userProfiles,
        analystsConnect: state.analysts,
        profilesLoaded,
        easyBookState: state.easybook,
        usedAvailabilityIds: state.supportConsultations.usedAvailabilityId,
        getAppointmentFromIdConnect: getAppointmentFromId.bind(
            null,
            state.analysts,
        ),
    };
}

const XlsJsonConnected = connect(mapStateToProps, {
    listenToUsedAvailabilityConnect: listenToUsedAvailability,
    fetchUserProfileConnect: fetchUserProfile,
    listenToAuthConnect: listenToAuth,
    listenToUserRootConnect: listenToUserRoot,
    listenToEasyBookConnect: listenToEasyBook,
    fetchAnalystsConnect: fetchAnalysts,
})(XlsJsonContainer);

export default XlsJsonConnected;
