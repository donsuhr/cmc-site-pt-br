import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import View from './View';
import ReduxDevToolsLoader from '../redux/ReduxDevToolsLoader';

const App = ({ store, ...props }) => (
    <Provider store={store}>
        <div>
            <View />
            <ReduxDevToolsLoader />
        </div>
    </Provider>
);

App.propTypes = {
    /* eslint-disable react/forbid-prop-types */
    store: PropTypes.object.isRequired,
};

export default App;
