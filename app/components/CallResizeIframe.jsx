import React from 'react';

class CallResizeIframe extends React.Component {
    componentDidMount() {
        setTimeout(() => {
            const data = JSON.stringify({
                type: 'screenload',
                height: window.document.body.scrollHeight + 26,
            });
            window.parent.postMessage(data, '*');
        }, 50);
    }

    render() {
        return <div style={{ display: 'none' }} />;
    }
}

export default CallResizeIframe;
