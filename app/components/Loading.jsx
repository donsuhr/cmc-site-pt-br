import React from 'react';
import PropTypes from 'prop-types';

class Loading extends React.Component {
    static propTypes = {
        children: PropTypes.node.isRequired,
    };

    componentDidMount() {
        const { parentNode } = this.node;
        const x = parentNode.offsetWidth / 2 - this.node.offsetWidth / 2;
        const y = Math.max(
            0,
            parentNode.offsetHeight / 2 - this.node.offsetHeight / 2,
        );
        this.node.style.left = `${x}px`;
        this.node.style.top = `${y}px`;
    }

    render() {
        const { children } = this.props;
        return (
            <div
                className="loading"
                ref={(node) => {
                    this.node = node;
                }}
            >
                {children}
            </div>
        );
    }
}

export default Loading;
