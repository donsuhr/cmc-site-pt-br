export const INIT_DOM_ATTRIBUTES = 'INIT_DOM_ATTRIBUTES';
export function initDomAttributes(data) {
    return {
        type: INIT_DOM_ATTRIBUTES,
        data,
    };
}
