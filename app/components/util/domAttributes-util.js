function getDomDataAttributes(el) {
    return [...el.attributes]
        .map(x => ({ name: x.name, value: x.value }))
        .filter(x => x.name.indexOf('data') !== -1)
        .reduce((accum, current) => {
            accum[current.name.replace('data-', '')] = current.value;
            return accum;
        }, {});
}

export default getDomDataAttributes;
