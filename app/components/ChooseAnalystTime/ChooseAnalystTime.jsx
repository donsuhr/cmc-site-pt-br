/* eslint-disable react/forbid-prop-types */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Loading from '../Loading';
import ShowError from '../ShowError';
import DayItemsList from './DayItemsList';
import CallResizeIframe from '../CallResizeIframe';

const ChooseAnalystTime = ({
    supportConsultations,
    availabilityByDay,
    getSupportConsultationConflicts,
    getScheduleItemIsAvailable,
    onTimeSelected,
    scheduledSessions,
    analyst,
    analystLoadStatus,
    fetching,
}) => {
    if (fetching) {
        return <Loading>Loading...</Loading>;
    }
    const showLoadError = analystLoadStatus
        && analystLoadStatus !== 'success'
        && analystLoadStatus !== '';
    return (
        <div className="support-consultations__select-analyst">
            {supportConsultations.fetching && <Loading>Saving...</Loading>}
            <h2 className="ht--s--18">Select A Time</h2>
            {showLoadError && <ShowError error={analystLoadStatus} />}
            <p>
                What time would you like to meet with
                {' '}
                {analyst.firstName}
                {' '}
                {analyst.lastName}
                {'.'}
            </p>
            <div className="support-consultations__select-analyst__days-container">
                {Object.keys(availabilityByDay).map((day) => (
                    <div
                        key={day}
                        className="support-consultations__select-analyst__day"
                    >
                        <h2 className="support-consultations__select-analyst__day-title">
                            {day}
                        </h2>
                        <DayItemsList
                            dayItems={availabilityByDay[day]}
                            getSupportConsultationConflicts={
                                getSupportConsultationConflicts
                            }
                            getScheduleItemIsAvailable={
                                getScheduleItemIsAvailable
                            }
                            onTimeSelected={onTimeSelected}
                            availabilityByDay={availabilityByDay}
                            scheduledSessions={scheduledSessions}
                        />
                    </div>
                ))}
            </div>
            <Link
                className="pill-button mt1 cmc-article__link--reversed pill-button--reversed"
                to="/choose-analyst"
            >
                Back
            </Link>
            <CallResizeIframe />
        </div>
    );
};

ChooseAnalystTime.propTypes = {
    supportConsultations: PropTypes.object,
    availabilityByDay: PropTypes.object,
    scheduledSessions: PropTypes.object,
    getSupportConsultationConflicts: PropTypes.func,
    getScheduleItemIsAvailable: PropTypes.func,
    onTimeSelected: PropTypes.func,
    analyst: PropTypes.object,
    fetching: PropTypes.bool,
    analystLoadStatus: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object,
    ]),
};

export default ChooseAnalystTime;
