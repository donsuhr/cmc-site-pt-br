/* eslint-disable react/forbid-prop-types */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchAnalysts } from '../ChooseAnalyst/actions';
import { availabilityByDay } from '../ChooseAnalyst/reducer';
import ChooseAnalystTime from './ChooseAnalystTime';
import { selectionComplete } from '../SupportConsultationsSignup2019/actions';
import { listenToAuth } from '../firebase/auth.actions';
import {
    addSupportCouncilToSchedule,
    listenToUsedAvailability,
} from '../firebase/schedule.support.actions';
import { getScheduleItemIsAvailable } from '../firebase/schedule.support.reducer';

export class ChooseAnalystTimeContainer extends React.Component {
    static propTypes = {
        analysts: PropTypes.object,
        analyst: PropTypes.object,
        user: PropTypes.object,
        supportConsultations: PropTypes.object,
        availabilityByDayConnect: PropTypes.object,
        fetchAnalystsConnect: PropTypes.func,
        listenToAuthConnect: PropTypes.func,
        addSupportCouncilToScheduleConnect: PropTypes.func,
        selectionCompleteConnect: PropTypes.func,
        getScheduleItemIsAvailableConnect: PropTypes.func,
        listenToUsedAvailabilityConnect: PropTypes.func,
        chosenAnalystId: PropTypes.string,
        history: PropTypes.object.isRequired,
    };

    state = {
        postingSelection: false,
    };

    componentWillMount() {
        const { chosenAnalystId, history } = this.props;
        if (!chosenAnalystId) {
            history.replace('/choose-analyst');
        }
    }

    componentDidMount() {
        const {
            analysts,
            fetchAnalystsConnect,
            listenToAuthConnect,
            listenToUsedAvailabilityConnect,
            history,
            user,
        } = this.props;
        if (analysts.hasEverLoaded === false && analysts.fetching === false) {
            fetchAnalystsConnect();
        }
        listenToAuthConnect().then(() => {
            listenToUsedAvailabilityConnect();
        });
        setTimeout(() => {
            if (!user.isAuth) {
                history.replace('/');
            }
        }, 1000);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const { user, history } = this.props;
        if (prevProps.user.isAuth !== user.isAuth && user.isAuth === false) {
            history.replace('/');
        }
    }

    componentWillUnmount() {
        window.scrollTo(0, 0);
    }

    onTimeSelected = (availabilityId, start, end) => {
        const {
            analyst,
            user,
            history,
            selectionCompleteConnect,
            addSupportCouncilToScheduleConnect,
        } = this.props;
        this.setState({
            postingSelection: true,
        });
        window.scrollTo(0, 0);

        addSupportCouncilToScheduleConnect({
            availabilityId,
            analyst,
            user,
            start,
            end,
        }).then((ref) => {
            history.push('/');
            selectionCompleteConnect();
        });
    };

    render() {
        const { postingSelection } = this.state;
        const {
            analyst,
            analysts,
            supportConsultations,
            availabilityByDayConnect,
            getScheduleItemIsAvailableConnect,
        } = this.props;

        const fetching = postingSelection
            || !analysts.hasEverLoaded
            || analysts.fetching
            || !supportConsultations.hasEverLoadedUsedAvailability;
        return (
            <ChooseAnalystTime
                fetching={fetching}
                analyst={analyst}
                analystLoadStatus={analysts.loadStatus}
                availabilityByDay={availabilityByDayConnect}
                supportConsultations={supportConsultations}
                onTimeSelected={this.onTimeSelected}
                getScheduleItemIsAvailable={getScheduleItemIsAvailableConnect}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        supportConsultations: state.supportConsultations,
        user: state.user,
        analysts: state.analysts,
        analyst: state.analysts.byId[state.ui.chosenAnalystId],
        availabilityByDayConnect: availabilityByDay(
            state.analysts,
            state.ui.chosenAnalystId,
        ),
        getScheduleItemIsAvailableConnect: getScheduleItemIsAvailable.bind(
            null,
            state.supportConsultations,
        ),
        chosenAnalystId: state.ui.chosenAnalystId,
        history: ownProps.history,
    };
}

const ChooseAnalystTimeConnected = connect(
    mapStateToProps,
    {
        fetchAnalystsConnect: fetchAnalysts,
        listenToAuthConnect: listenToAuth,
        listenToUsedAvailabilityConnect: listenToUsedAvailability,
        addSupportCouncilToScheduleConnect: addSupportCouncilToSchedule,
        selectionCompleteConnect: selectionComplete,
    },
)(ChooseAnalystTimeContainer);

export default ChooseAnalystTimeConnected;
