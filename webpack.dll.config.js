'use strict';

const webpack = require('webpack');
const path = require('path');
const parentDllEntry = require('cmc-site/webpack.dll.config').entry.dll;

const dll = parentDllEntry.concat([
    'firebase',
    'formsy-react',
    'moment-timezone',
    'react',
    'react-dom',
    'react-bootstrap',
    'react-redux',
    'react-router',
    'react-transition-group',
    'redux',
    'redux-thunk',
    'redux-devtools',
    'redux-devtools-dock-monitor',
    'redux-devtools-log-monitor',
    'moment',
]);

const production = process.env.NODE_ENV === 'production';
const mode = production ? 'production' : 'development';

module.exports = {
    mode,
    entry: {
        dll,
    },

    output: {
        filename: '[name].bundle.js',
        path: __dirname,
        // The name of the global variable which the library's
        // require() function will be assigned to
        library: '[name]_lib',
    },
    resolve: {
        modules: ['node_modules'],
        alias: {
            underscore: path.join(
                __dirname,
                'node_modules/underscore/underscore',
            ),
            handlebars: 'handlebars/runtime',
            jquery: 'jquery/src/jquery',
        },
    },

    plugins: [
        new webpack.ProvidePlugin({
            'window.jQuery': 'jquery', // garlic, headroom, bootstrap
            jQuery: 'jquery', // bootstrap parsley superfish
        }),
        new webpack.DllPlugin({
            // The path to the manifest file which maps between
            // modules included in a bundle and the internal IDs
            // within that bundle
            path: '[name]-manifest.json',
            // The name of the global variable which the library's
            // require function has been assigned to. This must match the
            // output.library option above
            name: '[name]_lib',
        }),
    ],
};
