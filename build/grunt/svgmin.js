module.exports = function svgmin(grunt, options) {
    return {
        dist: {
            files: [
                {
                    expand: true,
                    cwd: `${options.app}/images`,
                    src: '{,*/}*.svg',
                    dest: `${options.dist}/images`,
                },
            ],
        },
        sprite: {
            files: [
                {
                    expand: true,
                    cwd: `${options.modulesDir}/cmc-site/app/sprite-src-svg`,
                    src: '{,*/}*.svg',
                    dest: '.tmp/grunticon/src',
                },
                {
                    expand: true,
                    cwd: `${options.app}/sprite-src-svg`,
                    src: '{,*/}*.svg',
                    dest: '.tmp/grunticon/src',
                },
            ],
        },
    };
};
