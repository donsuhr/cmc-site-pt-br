'use strict';

const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const AssetsPlugin = require('assets-webpack-plugin');
const glob = require('glob');

const production = process.env.NODE_ENV === 'production';
const mode = production ? 'production' : 'development';

const entry = () => {
    const cmcSite = [path.join(__dirname, 'app/scripts/index.js')];
    if (!production) {
        cmcSite.unshift(
            'webpack/hot/dev-server',
            'webpack-hot-middleware/client?reload=true',
        );
    }

    return glob
        .sync('./app/scripts/pages/!(*.e2e).js', { absolute: true })
        .reduce(
            (acc, x) => {
                const ext = path.extname(x);
                acc[`page--${path.basename(x, ext)}`] = x;
                return acc;
            },
            {
                'cmc-site': cmcSite,
                'page--home': [
                    path.join(
                        __dirname,
                        'node_modules/cmc-site/app/scripts/pages/home',
                    ),
                ],
                'page--s3-doc-list': [
                    path.join(
                        __dirname,
                        'node_modules/cmc-site/app/scripts/pages/s3-doc-list',
                    ),
                ],
                'page--press-releases': [
                    path.join(
                        __dirname,
                        'node_modules/cmc-site/app/scripts/pages/press-releases',
                    ),
                ],
                'page--recent-news': [
                    path.join(
                        __dirname,
                        'node_modules/cmc-site/app/scripts/pages/recent-news',
                    ),
                ],

                'page--404': [
                    path.join(
                        __dirname,
                        'node_modules/cmc-site/app/scripts/pages/404',
                    ),
                ],
                'page--search': [
                    path.join(
                        __dirname,
                        'node_modules/cmc-site/app/scripts/pages/search',
                    ),
                ],
                'page--simple-form': [
                    path.join(
                        __dirname,
                        'node_modules/cmc-site/app/scripts/pages/simple-form',
                    ),
                ],
                'page--dl-case-study-form': [
                    path.join(
                        __dirname,
                        'node_modules/cmc-site/app/scripts/pages/dl-case-study-form',
                    ),
                ],
                'page--webcast-form': [
                    path.join(
                        __dirname,
                        'node_modules/cmc-site/app/scripts/pages/webcast-form',
                    ),
                ],
            },
        );
};

const plugins = [
    new AssetsPlugin({
        entrypoints: false,
        prettyPrint: true,
        filename: 'webpack-assets.json',
    }),
    new AssetsPlugin({
        entrypoints: true,
        prettyPrint: true,
        filename: 'webpack-manifest.json',
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
        'window.jQuery': 'jquery', // garlic, headroom, bootstrap
        jQuery: 'jquery', //  bootstrap parsley superfish
    }),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
            NETLIFY_ENV: JSON.stringify(
                process.env.NETLIFY_ENV || 'development',
            ),
        },
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/), // Ignore all optional deps of moment.js
];

if (!production) {
    plugins.push(new webpack.HotModuleReplacementPlugin());

    const manifestFile = path.resolve(process.cwd(), 'dll-manifest.json');
    let manifestJson;
    try {
        manifestJson = JSON.parse(fs.readFileSync(manifestFile));
    } catch (e) {
        manifestJson = false;
    }
    if (manifestJson) {
        plugins.push(
            new webpack.DllReferencePlugin({
                context: '.',
                manifest: manifestJson,
            }),
        );
    }
}

module.exports = {
    stats: 'errors-only',
    devtool: production ? 'source-map' : '#eval-source-map',
    context: path.join(__dirname, 'app', 'scripts'),
    entry,
    mode,
    plugins,
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            maxInitialRequests: Infinity,
            maxAsyncRequests: Infinity,
            minSize: 11000,
            chunks: 'all',
        },
    },
    resolve: {
        modules: ['node_modules', path.resolve(__dirname, 'app')],
        alias: {
            config: path.join(__dirname, 'config.js'),
            underscore: path.join(
                __dirname,
                'node_modules/underscore/underscore',
            ),
            handlebars: 'handlebars/runtime',
            jquery: 'jquery/src/jquery',
        },
        extensions: ['.js', '.jsx'],
    },
    output: {
        path: path.join(__dirname, 'dist/'),
        filename: production
            ? 'scripts/[name]-[chunkhash].js'
            : 'scripts/[name].js',
        chunkFilename: production
            ? 'scripts/[name]-[chunkhash].js'
            : 'scripts/[name].js',
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /nls[/\\]\S*\.json/,
                use: [
                    {
                        loader: 'amdi18n-loader',
                    },
                ],
                include: [
                    path.join(__dirname, '/app/components/'),
                    path.join(__dirname, '/node_modules/cmc-site/app/'),
                ],
            },
            {
                test: /\.jsx?$/,
                use: ['babel-loader'],
                include: [
                    path.join(__dirname, '/app/'),
                    path.join(__dirname, '/config.js'),
                    path.join(__dirname, '/node_modules/autotrack'),
                    path.join(__dirname, '/node_modules/dom-utils'),
                    /\/node_modules\/cmc-/,
                ],
            },
            {
                test: /\.hbs$/,
                use: [
                    {
                        loader: 'handlebars-loader',
                        query: {
                            debug: false,
                            runtime: 'handlebars/runtime',
                            helperDirs: [
                                `${__dirname}/app/metalsmith/helpers`,
                                `${__dirname}/node_modules/cmc-site/app/metalsmith/helpers`,
                            ],
                            partialDirs: [
                                `${__dirname}/app/metalsmith/partials`,
                                `${__dirname}/node_modules/cmc-site/app/metalsmith/partials`,
                            ],
                        },
                    },
                ],
                include: [
                    path.join(__dirname, '/app/'),
                    path.join(__dirname, '/node_modules/cmc-site'),
                ],
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'underscore-template-loader',
                        query: {
                            variable: 'data',
                        },
                    },
                ],
            },
        ],
    },
};
