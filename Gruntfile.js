'use strict';

const timeGrunt = require('time-grunt');
const loadGruntConfig = require('load-grunt-config');
const path = require('path');
const loadDotEnv = require('cmc-load-dot-env');

if (!{}.hasOwnProperty.call(process.env, 'AWS_ACCESS_KEY_ID__CMC_DEMOCENTER_DL')) {
    // eslint-disable-next-line no-console
    console.log('process.env.AWS_ACCESS_KEY_ID__CMC_DEMOCENTER_DL not found, loading .env');
    loadDotEnv();
}

const modulesDir = path.resolve(require.resolve('cmc-load-dot-env'), '../..');

module.exports = function gruntfile(grunt) {
    const config = {
        app: 'app',
        dist: 'dist',
        sassOutput: '.tmp/sass-out/',
        livereloadPort: 35729,
        testLiveReload: 35730,
        modulesDir,
    };

    timeGrunt(grunt);

    loadGruntConfig(grunt, {
        configPath: path.join(modulesDir, 'cmc-site/build/grunt'),
        overridePath: path.join(process.cwd(), '/build/grunt'),
        data: config,
        jitGrunt: {
            staticMappings: {
                sprite: 'grunt-spritesmith',
                scsslint: 'grunt-scss-lint',
            },
        },
    });

    grunt.registerTask('serve', (target) => {
        if (target === 'dist') {
            grunt.task.run([
                'build',
                'connect:dist:keepalive',
            ]);
        } else {
            grunt.task.run([
                'exec:check-npm',
                'clean',
                'exec:write-config',
                'exec:build-js',
                'exec:metalsmith-dist',
                'exec:sass',
                'exec:postcss',
                'connect:livereload',
                'concurrent:serve',
            ]);
        }
    });

    grunt.registerTask('test', (target) => {
        if (target !== 'fromWatch') {
            grunt.task.run([
                // 'clean'
            ]);
        }

        grunt.task.run([
            'connect:test',
            'mocha',
        ]);
    });

    grunt.registerTask('testServer', () => {
        grunt.task.run([
            'connect:testServer',
            'watch:testServer',
        ]);
    });

    grunt.registerTask('build', [
        'clean',
        'exec:write-config',
        'concurrent:dist',
        'exec:write-sitemap',
        'exec:metalsmith-dist',
        'exec:critical-css',
        'exec:postcss-min',
        'filerev',
        'usemin',
        'exec:inline',
        'htmlmin',
    ]);

    grunt.registerTask('sprites', [
        'concurrent:sprite1',
        'concurrent:sprite2',
    ]);

    grunt.registerTask('default', [
        'test',
        'build',
    ]);

    grunt.registerTask('styles', [
        'exec:sass',
        'exec:critical-css',
        'exec:postcss-min',
        'copy:dist',
        'filerev',
        'notify:styles',
    ]);

    grunt.registerTask('pages', [
        'exec:write-config',
        'exec:write-sitemap',
        'exec:metalsmith-dist',
        'usemin',
        'exec:inline',
    ]);

    grunt.registerTask('dist', ['build', 'notify:dist']);

    grunt.registerTask('netlify', [
        'build',
        'build-search-index',
    ]);

    grunt.registerTask('build-search-index', () => {
        if (process.env.BRANCH !== 'netlify') {
            // eslint-disable-next-line no-console
            console.log('not "netlify" branch, skipping build index');
            return;
        }
        grunt.task.run([
            'aws_s3:search-index-json--get',
            'exec:build-search-index',
            'aws_s3:search-index-json--put',
        ]);
    });
};
